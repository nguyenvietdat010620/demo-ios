////
////  CameraConfig.swift
////  demo-ios
////
////  Created by Nguyen Viet Dat on 27/11/2023.
////
//
//import Foundation
//import AVFoundation
//
//class CameraController {
//    let captureSession = AVCaptureSession()
//
//    // Find the default audio device.
//    guard let audioDevice = AVCaptureDevice.default(for: .audio) else { return }
//
//
//    do {
//        // Wrap the audio device in a capture device input.
//        let audioInput = try AVCaptureDeviceInput(device: audioDevice)
//        // If the input can be added, add it to the session.
//        if captureSession.canAddInput(audioInput) {
//            captureSession.addInput(audioInput)
//        }
//    } catch {
//        // Configuration failed. Handle error.
//    }
//}
