//
//  CollectionViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 04/12/2023.
//

import UIKit

class CollectionViewController: UIViewController, UICollectionViewDataSource {
    

    @IBOutlet weak var studyCollection: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "TestStudyCollectionViewCell", bundle: nil)
        studyCollection.register(nib, forCellWithReuseIdentifier: "TestStudyCollectionViewCell")
        
        studyCollection.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = studyCollection.dequeueReusableCell(withReuseIdentifier: "TestStudyCollectionViewCell", for: indexPath as IndexPath) as! TestStudyCollectionViewCell
        cell.viewCell.backgroundColor = UIColor.random
        return cell
    }
    
}

extension CollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 20)
    }
    
    
}
