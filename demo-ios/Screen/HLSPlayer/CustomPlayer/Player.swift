//
//  Player.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 13/12/2023.
//

import Foundation
import AVKit
import AVFoundation

class Player: UIView {
    
    var kvoRateContext = 0
    var avPlayer: AVPlayer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setUpUIPlayPause() {
        var view = UIView()
        view.frame = CGRect(x: 0, y: 100, width: 100, height: 100)
        view.backgroundColor = UIColor.green
        var image = UIImageView(image: UIImage(named: "doctor"))
        
        view.addSubview(image)
    }
    
}
