//
//  HLSPlayerViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 27/11/2023.
//

import UIKit
import AVFoundation
import AVKit

class HLSPlayerViewController: UIViewController {
    
    let movieURL = "https://vod-zlr2.tv360.vn/video1/2022/05/31/14/93d9fa7d/93d9fa7d-58ed-47e7-879a-0eaae285c2a6_1_.m3u8"
    var playPause: Player!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        let videoPath = "https://vod-zlr2.tv360.vn/video1/2022/05/31/14/93d9fa7d/93d9fa7d-58ed-47e7-879a-0eaae285c2a6_1_.m3u8"
        guard let url = URL(string: videoPath) else { return }
        let player = AVPlayer(url: url)
        player.rate = 1 //auto play
        let playerFrame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        playerViewController.view.frame = playerFrame
        
        playerViewController.showsPlaybackControls = false
        
        addChild(playerViewController)
        view.addSubview(playerViewController.view)
        
        playPause = Player()
        playPause.avPlayer = player
        view.addSubview(playPause)
        playPause.setUpUIPlayPause()
        
        playerViewController.didMove(toParent: self)
    }
    
    
}
