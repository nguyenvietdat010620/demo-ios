//
//  HomeViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 31/10/2023.
//

import UIKit
import Alamofire
import SQLite
import SQLite3

extension HomeViewController: Navigation {
    func navigationToScreen<T>(screenType: ScreenType, data: T?) {
        switch screenType {
        case .Doctor:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DoctorViewController") as! DoctorViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .News:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NewsViewController") as! NewsViewController
            vc.objNew = data as? ModalArtical
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .Detail:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .Image:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
            vc.promotionItem = data as? PromotionHome
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        default:
            break
        }
    }
}

extension HomeViewController: PassContentOffSet {
    func passContentOffSet(content: [ContentOffSet]) {
        collectionViewOffsets = content
    }
    
}


class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var db: OpaquePointer?
    var listPromotion: [PromotionHome] = []
    var listArtcal: [ModalArtical] = []
    var listDoctor: [ModalDoctorHome] = []
    var responseHome: ResponseHome?
    var collectionViewOffsets: [ContentOffSet] = []
    var rowNeedToUpdate: Int = 0
    
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var labelNameUser: UILabel!
    @IBOutlet weak var statusAction: UIView!
    @IBOutlet weak var buttonAvata: UIButton!
    @IBOutlet weak var textStatus: UILabel!
    @IBOutlet weak var tableViewHome: UITableView!
    @IBOutlet weak var viewTable: UIView!
    @IBOutlet weak var avataHome: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "HomeTableViewCell", bundle: nil)
        tableViewHome.register(nib, forCellReuseIdentifier: "HomeTableViewCell")
        
        let nib2 = UINib(nibName: "TestScrollTableViewCell", bundle: nil)
        tableViewHome.register(nib2, forCellReuseIdentifier: "TestScrollTableViewCell")
        // Do any additional setup after loading the view.
        tableViewHome.dataSource = self
        tableViewHome.delegate = self
        tableViewHome!.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        tableViewHome.separatorStyle = .none
        imagePicker.delegate = self
        viewTable.layer.cornerRadius = 10
        Loading.shared.showLoading(view: self.view)
        getData()
        
        avataHome.layer.cornerRadius = 21
        avataHome.layer.borderWidth = 1
        avataHome.layer.borderColor = UIColor.white.cgColor
        
        labelNameUser.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        labelNameUser.font = UIFont(name:"HelveticaNeue-Bold", size: 18)
        
        textStatus.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.8)
        textStatus.font = UIFont(name:"HelveticaNeue", size: 12)
        statusAction.roundCorners(.allCorners, radius: 4)
        viewTable.backgroundColor = .white
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        avataHome.isUserInteractionEnabled = true
        avataHome.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        //        insertDataDoctor()
        //        insertDataArtical()
        //        insertDataPromotion()
        //        selectDoctor()
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
        // Your action
    }
    
    func getData(){
        AF.request("https://gist.githubusercontent.com/hdhuy179/f967ffb777610529b678f0d5c823352a/raw", method: .get)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let data):
                    do {
//                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { time in
                            Loading.shared.hideOverlayView()
//                        }
                        let jsonData = try JSONSerialization.data(withJSONObject: data)
                        self.responseHome = try JSONDecoder().decode(ResponseHome.self, from: jsonData)
                        self.listArtcal = self.responseHome?.data?.articleList ?? []
                        self.listPromotion = self.responseHome?.data?.promotionList ?? []
                        self.listDoctor = self.responseHome?.data?.doctorList ?? []
                        DispatchQueue.main.async {
                            self.tableViewHome.reloadData()
                        }
                        
                        
                    } catch {
                        print("Error decoding JSON: \(error)")
                    }
                case .failure(let err):
                    print("data" , err)
                default:
                    print("false")
                }
            }
    }
    
    func insertDataDoctor() {
        for (_, item) in listDoctor.enumerated() {
            databaseSQLite.shared.insertIntoTableDoctor(id: item.id, full_name: item.full_name, name: item.name, last_name: item.last_name, contact_email: item.contact_email, phone: item.phone, avatar: item.avatar ?? "", majors_name: item.majors_name, ratio_star: item.ratio_star, number_of_reviews: item.number_of_reviews, number_of_stars: item.number_of_stars)
        }
    }
    
    func insertDataArtical() {
        for (_, item) in listArtcal.enumerated() {
            databaseSQLite.shared.insertIntoTableArtical(id: item.id, category_id: item.category_id, title: item.title, slug: item.slug, picture: item.picture, picture_caption: item.picture_caption ?? "", created_at: item.created_at, category_name: item.category_name, link: item.link)
        }
    }
    
    func insertDataPromotion() {
        for (_, item) in listPromotion.enumerated() {
            databaseSQLite.shared.insertIntoTablePromotion(id: item.id, category_id: item.category_id, code: item.code, name: item.name, slug: item.slug, content: item.content, picture: item.picture, from_date: item.from_date, to_date: item.to_date, amount: item.amount, type: item.type, kind: item.kind, created_at: item.created_at, category_name: item.category_name, amount_text: item.amount_text, link: item.link, is_bookmark: item.is_bookmark)
        }
    }
    
    func selectDoctor() {
        databaseSQLite.shared.selectDoctor()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1000
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row < 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath as IndexPath) as! HomeTableViewCell
            for item in collectionViewOffsets{
                if(indexPath.row == item.row){
                    cell.collectionViewIntable.contentOffset = item.contentOffset
                }
            }
            switch indexPath.row {
            case 0:
                cell.listArtcal = listArtcal
                cell.number = listArtcal.count
                cell.labelTitle.text = "Tin tức"
                cell.index = 0
                cell.reloadWhenSend()
            case 1:
                cell.listPromotion = listPromotion
                cell.number = listPromotion.count
                cell.labelTitle.text = "Khuyến mãi"
                cell.index = 1
                cell.delegateNavigation = self
                cell.reloadWhenSend()
            case 2:
                cell.listDoctor = listDoctor
                cell.number = listDoctor.count
                cell.labelTitle.text = "Giới thiệu bác sĩ"
                cell.index = 2
                cell.delegateNavigation = self
                cell.reloadWhenSend()
            default:
                print()
            }
            cell.listViewOffSet = collectionViewOffsets
            cell.deleegateContentOffSet = self
            cell.labelTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 17)
//            cell.reloadWhenSend()
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath as IndexPath) as! HomeTableViewCell
            for item in collectionViewOffsets{
                if(indexPath.row == item.row){
                    cell.collectionViewIntable.contentOffset = item.contentOffset
                }
            }
            cell.listViewOffSet = collectionViewOffsets
            cell.deleegateContentOffSet = self
            cell.listDoctor = listDoctor
            cell.number = listArtcal.count
            cell.labelTitle.text = "Bác sĩ"
            cell.index = indexPath.row
            cell.reloadWhenSend()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 265
    }
    
    
    
    @IBAction func actionAvata(_ sender: Any) {
        
        //        insertDataDoctor()
        //        insertDataArtical()
        //        insertDataPromotion()
        //        selectDoctor()
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let resizedImage = resizeImage(image: selectedImage, targetSize: CGSize(width: 42, height: 42))
            
            avataHome.image = resizedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        let widthRatio = targetSize.width / size.width
        let heightRatio = targetSize.height / size.height
        
        var newSize: CGSize
        if widthRatio > heightRatio {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}

extension UIView {
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}



