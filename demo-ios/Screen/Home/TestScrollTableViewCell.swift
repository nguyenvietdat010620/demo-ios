//
//  TestScrollTableViewCell.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 28/11/2023.
//

import UIKit

class TestScrollTableViewCell: UITableViewCell {

    @IBOutlet weak var switchtoggle: UISwitch!
    @IBOutlet weak var labelTest: UILabel!
    @IBOutlet weak var iamgeTest: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
            // invoke superclass implementation
            super.prepareForReuse()
            
        self.switchtoggle.isOn = true

        }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

