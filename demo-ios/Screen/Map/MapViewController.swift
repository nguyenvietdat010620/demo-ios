//
//  MapViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 16/11/2023.
//

import UIKit
import GoogleMaps
class MapViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupMapView()
        UIButtonBack()
        // Do any additional setup after loading the view.
    }
    
    func setupMapView() {
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 12.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        mapView.settings.zoomGestures = true
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = mapView
        
        view = mapView
    }
    
    func UIButtonBack() {
        let floatingButton = UIButton(type: .custom)
        floatingButton.setImage(UIImage(named: "back"), for: .normal)
//        floatingButton.setTitle("Your Button", for: .normal)
        floatingButton.backgroundColor = .none
        floatingButton.frame = CGRect(x: 16, y: 30, width: 40, height: 40)
        floatingButton.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        view.addSubview(floatingButton)
//        self.view.addSubview(view)
    }
    
    @objc func buttonTapped() {
           // Handle button tap
           print("Button tapped!")
       }
}
