//
//  TestViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 24/11/2023.
//

import UIKit
import Alamofire

class TestViewController: UIViewController {
    
    var images = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
        // Do any additional setup after loading the view.
    }
    
    func getData(){
        NetworkService.shared.getUserInfor { [weak self] result in
            print("err", "loi ne", result)
            switch result {
            case .success(let data):
                print(data)
            case .failure(let err):
                if let error1 = err as? AFError{
                    switch error1 {
                    
                    case .requestRetryFailed(let retryError, let originalError):
                        print("retryError", retryError, originalError )
                        NetworkError.shared.showErrorNetwork(view: (self?.view)!)
                    default:
                        break
                    }
                }
                default:
                    print()
                }
            }
            
        }
        
    }
    
    

