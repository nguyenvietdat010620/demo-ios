//
//  ImageViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 18/11/2023.
//

import UIKit

class ImageViewController: UIViewController {
    var promotionItem: PromotionHome?
    @IBOutlet weak var imageMain: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        var imageURL = promotionItem?.picture ?? ""
        ImageLoader.loadImage(from: imageURL) { image in
            DispatchQueue.main.async {
                self.imageMain.image = image
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionDownload(_ sender: Any) {
        if let imageURL = URL(string: promotionItem?.picture ?? "") {
            downloadImageFromURL(url: imageURL) { image in
                if let image = image {
                    self.saveImageToPhotoAlbum(image: image)
                    print("Image saved to photo album.")
                } else {
                    print("Failed to download or convert image.")
                }
            }
        } else {
            print("Invalid URL")
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    

    func downloadImageFromURL(url: URL, completion: @escaping (UIImage?) -> Void) {
        let task = URLSession.shared.downloadTask(with: url) { (tempURL, response, error) in
            guard let tempURL = tempURL, error == nil else {
                completion(nil)
                return
            }

            if let data = try? Data(contentsOf: tempURL),
               let image = UIImage(data: data) {
                completion(image)
            } else {
                completion(nil)
            }
        }
        
        task.resume()
    }
    func saveImageToPhotoAlbum(image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
    }
    
}
