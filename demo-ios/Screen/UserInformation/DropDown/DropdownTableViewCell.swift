//
//  DropdownTableViewCell.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 21/11/2023.
//

import UIKit
import DropDown



class DropdownTableViewCell: UITableViewCell {
    
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var dropdownbtn: UIButton!
    
    let dropDown = DropDown()
    var blood:String?
    let data = ["A+", "B+", "AB", "O"]
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        dropDown.anchorView = dropdownbtn
        dropDown.dataSource = data
        dropDown.width = 200
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
//                    self.selectedValueLabel.text = item
            print("item", item)
            blood = item
            textfield.text = item
                }
//        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height ?? 0))
        dropDown.bottomOffset = CGPoint(x: 200, y: 200)
    }

    @IBAction func btnDropdown(_ sender: Any) {
        dropDown.show()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
