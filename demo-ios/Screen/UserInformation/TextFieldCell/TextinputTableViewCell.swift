//
//  TextinputTableViewCell.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 21/11/2023.
//

import UIKit
protocol GetdatafromCell {
    func getDatafromCell(data : String)
}

class TextinputTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var labelTitle: UILabel!
    
    var delegaCell: GetdatafromCell?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textfield.delegate = self
        // Initialization code
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        delegaCell?.getDatafromCell(data: textfield.text ?? "")
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
