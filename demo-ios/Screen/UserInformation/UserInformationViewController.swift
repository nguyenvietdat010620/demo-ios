//
//  UserInformationViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 21/11/2023.
//

import UIKit

class GetPhone: GetdatafromCell {
    func getDatafromCell(data: String) {
        print("data change", data)
    }
}
class GetName: GetdatafromCell {
    func getDatafromCell(data: String) {
        print("data GetName", data)
    }
}
class GetLastName: GetdatafromCell {
    func getDatafromCell(data: String) {
        print("data GetName", data)
    }
}
class GetGender: GetdataSwithGender {
    func getDatafromSwithGender(data: String) {
        print("getDatafromSwithGender", data)
    }
}


class UserInformationViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var name: String?
    let dataTitle = [
        [
            "title" : "Tên",
            "placeHoder" : "Nhập tên của bạn"
        ]
        ,
        [
            "title" : "Họ",
            "placeHoder" : "Nhập họ của bạn"
        ],
        [
            "title" : "Ngày sinh",
            "placeHoder" : "DD/MM/YYYY"
        ],
        [
            "title" : "Giới tính",
            "placeHoder" : ""
        ],
        [
            "title" : "Số điện thoại",
            "placeHoder" : "Nhập số điện thoại của bạn"
        ],
        [
            "title" : "Email",
            "placeHoder" : "Nhập số điện thoại của bạn"
        ],
        [
            "title" : "Tỉnh /Thành phố",
            "placeHoder" : "Chưa cập nhật"
        ],
        [
            "title" : "Quận/Huyện",
            "placeHoder" : "Chưa cập nhật"
        ],
        [
            "title" : "Phường xã",
            "placeHoder" : "Chưa cập nhật"
        ],
        [
            "title" : "Địa chỉ nơi ở",
            "placeHoder" : "Nơi thường trú của bạn"
        ],
        [
            "title" : "Nhóm máu",
            "placeHoder" : "A+/B+/AB/O"
        ]
    ]
    var dataUser: UserResponse?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "DropdownTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "DropdownTableViewCell")
        let nib1 = UINib(nibName: "TextinputTableViewCell", bundle: nil)
        tableView.register(nib1, forCellReuseIdentifier: "TextinputTableViewCell")
        let nib2 = UINib(nibName: "SwitchTableViewCell", bundle: nil)
        tableView.register(nib2, forCellReuseIdentifier: "SwitchTableViewCell")
        let nib3 = UINib(nibName: "ContinueTableViewCell", bundle: nil)
        tableView.register(nib3, forCellReuseIdentifier: "ContinueTableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        
        
//        tableView.separatorStyle = .none
        // Do any additional setup after loading the view.
        getData()
        Loading.shared.showLoading(view: self.view)
    }
    
    func getData() {
        UserService.shared.getUserInfor { [weak self] result in
            switch result{
            case .success(let data):
                self?.dataUser = data
                self?.tableView.reloadData()
//                print(data)
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { time in
                    Loading.shared.hideOverlayView()
                }
            case .failure(let err):
                print()
            }
        }
    }
}

extension UserInformationViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataTitle.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextinputTableViewCell", for: indexPath as IndexPath) as! TextinputTableViewCell
            cell.labelTitle.text = dataTitle[indexPath.row]["title"]
            cell.textfield.borderStyle = UITextField.BorderStyle.none
            cell.textfield.placeholder = dataTitle[indexPath.row]["placeHoder"]
            cell.textfield.text = dataUser?.data.name
            cell.delegaCell = GetName()
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextinputTableViewCell", for: indexPath as IndexPath) as! TextinputTableViewCell
            cell.labelTitle.text = dataTitle[indexPath.row]["title"]
            cell.textfield.placeholder = dataTitle[indexPath.row]["placeHoder"]
            cell.textfield.borderStyle = UITextField.BorderStyle.none
            cell.textfield.text = dataUser?.data.last_name
            cell.delegaCell = GetLastName()
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DropdownTableViewCell", for: indexPath as IndexPath) as! DropdownTableViewCell
            cell.labelTitle.text = dataTitle[indexPath.row]["title"]
            cell.textfield.placeholder = dataTitle[indexPath.row]["placeHoder"]
            cell.textfield.borderStyle = UITextField.BorderStyle.none
            cell.textfield.text = dataUser?.data.birth_date
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchTableViewCell", for: indexPath as IndexPath) as! SwitchTableViewCell
            cell.labelTitle.text = dataTitle[indexPath.row]["title"]
            cell.delegate = GetGender()
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextinputTableViewCell", for: indexPath as IndexPath) as! TextinputTableViewCell
            cell.labelTitle.text = dataTitle[indexPath.row]["title"]
            cell.textfield.placeholder = dataTitle[indexPath.row]["placeHoder"]
            cell.textfield.borderStyle = UITextField.BorderStyle.none
            cell.textfield.text = dataUser?.data.phone
            cell.delegaCell = GetName()
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextinputTableViewCell", for: indexPath as IndexPath) as! TextinputTableViewCell
            cell.labelTitle.text = dataTitle[indexPath.row]["title"]
            cell.textfield.placeholder = dataTitle[indexPath.row]["placeHoder"]
            cell.textfield.borderStyle = UITextField.BorderStyle.none
            cell.textfield.text = dataUser?.data.contact_email
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DropdownTableViewCell", for: indexPath as IndexPath) as! DropdownTableViewCell
            cell.labelTitle.text = dataTitle[indexPath.row]["title"]
            cell.textfield.placeholder = dataTitle[indexPath.row]["placeHoder"]
            cell.textfield.borderStyle = UITextField.BorderStyle.none

            return cell
        case 7:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DropdownTableViewCell", for: indexPath as IndexPath) as! DropdownTableViewCell
            cell.labelTitle.text = dataTitle[indexPath.row]["title"]
            cell.textfield.placeholder = dataTitle[indexPath.row]["placeHoder"]
            cell.textfield.borderStyle = UITextField.BorderStyle.none

            return cell
        case 8:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DropdownTableViewCell", for: indexPath as IndexPath) as! DropdownTableViewCell
            cell.labelTitle.text = dataTitle[indexPath.row]["title"]
            cell.textfield.placeholder = dataTitle[indexPath.row]["placeHoder"]
            cell.textfield.borderStyle = UITextField.BorderStyle.none

            return cell
        case 9:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DropdownTableViewCell", for: indexPath as IndexPath) as! DropdownTableViewCell
            cell.labelTitle.text = dataTitle[indexPath.row]["title"]
            cell.textfield.placeholder = dataTitle[indexPath.row]["placeHoder"]
            cell.textfield.borderStyle = UITextField.BorderStyle.none

            return cell
        case 10:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DropdownTableViewCell", for: indexPath as IndexPath) as! DropdownTableViewCell
            cell.labelTitle.text = dataTitle[indexPath.row]["title"]
            cell.textfield.placeholder = dataTitle[indexPath.row]["placeHoder"]
            cell.textfield.borderStyle = UITextField.BorderStyle.none
            cell.textfield.text = dataUser?.data.blood_name
            
            return cell
        case 11:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContinueTableViewCell", for: indexPath as IndexPath) as! ContinueTableViewCell
            cell.doneButton.backgroundColor = UIColor(red: 44/256, green: 134/256, blue: 103/256, alpha: 1)
            cell.doneButton.layer.cornerRadius = 21
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextinputTableViewCell", for: indexPath as IndexPath) as! TextinputTableViewCell
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 3){
            return 80
        }
        else{
            return 70
        }
    }
}

