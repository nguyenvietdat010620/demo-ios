//
//  ContinueTableViewCell.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 21/11/2023.
//

import UIKit

class ContinueTableViewCell: UITableViewCell {

    @IBOutlet weak var doneButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
