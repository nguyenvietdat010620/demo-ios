//
//  SwitchTableViewCell.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 21/11/2023.
//

import UIKit

protocol GetdataSwithGender {
    func getDatafromSwithGender(data : String)
}

class SwitchTableViewCell: UITableViewCell {

    @IBOutlet weak var viewRight: UIView!
    @IBOutlet weak var viewLeft: UIView!
    @IBOutlet weak var viewSwitchBackground: UIView!
    @IBOutlet weak var spaceMale: NSLayoutConstraint!
    @IBOutlet weak var spaceFemale: NSLayoutConstraint!
    @IBOutlet weak var widthBtn: NSLayoutConstraint!
    @IBOutlet weak var labelTitle: UILabel!
    let screenSize = UIScreen.main.bounds
    var isFemale = true
    var delegate: GetdataSwithGender?
    override func awakeFromNib() {
        super.awakeFromNib()
        viewSwitchBackground.backgroundColor = UIColor(red: 244/256, green: 245/256, blue: 247/256, alpha: 1)
        // Initialization code
        widthBtn.constant = screenSize.width / 2 - 12
        spaceMale.constant = (screenSize.width / 2 - 10) / 3
        spaceFemale.constant = (screenSize.width / 2 - 10) / 3
        
        setupViewBorder()
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        self.viewLeft.addGestureRecognizer(gesture)
        
        let gesture1 = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction2))
        self.viewRight.addGestureRecognizer(gesture1)
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        print("1")
        switchButton()
        delegate?.getDatafromSwithGender(data: "Male")
    }
    
    @objc func checkAction2(sender : UITapGestureRecognizer) {
        print("2")
        switchButton()
        delegate?.getDatafromSwithGender(data: "Female")
    }
    
    
    func switchButton(){
        if(isFemale){
            isFemale = !isFemale
            viewLeft.backgroundColor = UIColor.white
            viewRight.backgroundColor = .none
        }
        else{
            viewLeft.backgroundColor = .none
            viewRight.backgroundColor = UIColor.white
            isFemale = !isFemale
        }
    }
    
    func setupViewBorder() {
        viewLeft.layer.cornerRadius = 8
        viewRight.layer.cornerRadius = 8
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
