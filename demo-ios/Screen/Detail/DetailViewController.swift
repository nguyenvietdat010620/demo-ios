//
//  DetailViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 14/11/2023.
//

import UIKit
import RealmSwift

class DetailViewController: UIViewController {
    var dataDetail: ResponseDetail?
    
    @IBOutlet weak var labelDes: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var shareButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//            getData()
            setupUI()
        Loading.shared.showLoading(view: self.view)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupUI() {
        let realm = try! Realm()
        let list = realm.objects(DetailModalRealm.self)
        
        if(list.count > 1){
            let imageURL = list[0].picture
            ImageLoader.loadImage(from: imageURL) { imag in
                DispatchQueue.main.async {
                    self.image.image = imag
                }
            }
            labelTitle.text = list[0].name
            labelTitle.font = UIFont(name:"NunitoSans-Bold", size: 20)
            labelDate.text = list[0].created_at
            labelDate.font = UIFont(name:"NunitoSans", size: 12)
            labelDate.textColor = UIColor(red: 125/256, green: 132/256, blue: 152/256, alpha: 1)
            labelDes.text = list[0].content.htmlToString
        }
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { time in
            Loading.shared.hideOverlayView()
        }
    }
    
    
    
    @IBAction func shareAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserInformationViewController") as! UserInformationViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
    }
    
}

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        return array
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
