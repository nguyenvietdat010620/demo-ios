//
//  SettingViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 03/12/2023.
//

import UIKit

class SettingViewController: UIViewController {
   
    

    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        tableview.dataSource = self
        let nib = UINib(nibName: "SettingTableViewCell", bundle: nil)
        tableview.register(nib, forCellReuseIdentifier: "SettingTableViewCell")
        tableview.backgroundColor = UIColor(red: 235/256, green: 250/256, blue: 250/256, alpha: 1)
        tableview.layoutMargins =  UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        // Do any additional setup after loading the view.
    }
    

}

extension SettingViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell", for: indexPath as IndexPath) as! SettingTableViewCell
        cell.layer.cornerRadius = 10
        
        return cell
    }
}
