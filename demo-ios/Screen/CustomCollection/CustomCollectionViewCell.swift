//
//  CustomCollectionViewCell.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 01/12/2023.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var viewCustomCell: UIView!
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
