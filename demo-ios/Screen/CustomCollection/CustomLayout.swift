//
//  CustomLayout.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 01/12/2023.
//

import Foundation
import UIKit

protocol PinterestLayoutDelegate: AnyObject {
    func collectionView(
        _ collectionView: UICollectionView,
        heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
}

class CustomLayout: UICollectionViewLayout {
    let screen = UIScreen.main.bounds
    var delegateCustom: PinterestLayoutDelegate!
    let attributeArray = NSMutableDictionary()
    var contentSize:CGSize!
    let numberOfColumn : Int = 2
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepare() {
        super.prepare()
        let padding:CGFloat = 15.0;
        let collectionViewWidth = self.collectionView?.frame.size.width
        let itemWidth : CGFloat = (collectionViewWidth! - padding * CGFloat((numberOfColumn + 1))) / CGFloat(numberOfColumn)
        var contentHeight:CGFloat = 0.0;
        var columnArray = [CGFloat](repeating: 0.0, count: numberOfColumn);
        for i in 0 ... (self.collectionView?.numberOfItems(inSection: 0))! - 1 {
            var tempX : CGFloat = 0.0
            var tempY : CGFloat = 0.0
            let indexPath = NSIndexPath(item: i, section: 0)
            let itemHeight:CGFloat = delegateCustom.collectionView((self.collectionView)!, heightForPhotoAtIndexPath: indexPath as IndexPath)
            
            //Tìm cột có độ dài ngắn nhất trong CollectionView
            var minHeight:CGFloat = 0.0;
            var minIndex:Int = 0;
            
            if (numberOfColumn > 0){
                minHeight = columnArray[0]
                
            }
            for colIndex in 0..<numberOfColumn {
                if (minHeight > columnArray[colIndex]){
                    minHeight = columnArray[colIndex]
                    minIndex = colIndex
                }
            }
            
            //Bổ sung  cell mới vào cột có kích thước ngắn nhất
            tempX = padding + (itemWidth + padding) *  CGFloat(minIndex);
            tempY = minHeight + padding;
            columnArray[minIndex] = tempY + itemHeight;
            let attributes = UICollectionViewLayoutAttributes(forCellWith:indexPath as IndexPath);
            attributes.frame = CGRect(x: tempX, y: tempY, width: itemWidth, height: itemHeight);
            self.attributeArray.setObject(attributes, forKey: indexPath)
            
            //Tính toán lại chiều cao Content Size của CollectionView
            let newContentHeight:CGFloat = tempY + padding + itemHeight + padding;
            if (newContentHeight > contentHeight){
                contentHeight = newContentHeight;
            }
        }
        
        self.contentSize = CGSize(width: (self.collectionView?.frame.size.width)!, height: contentHeight);
    }
    
    override var collectionViewContentSize: CGSize {
        print("collectionViewContentSize")
        return contentSize
    }
    
    
    override func layoutAttributesForElements(in rect: CGRect)
        -> [UICollectionViewLayoutAttributes]? {
            var layoutAttributes = [UICollectionViewLayoutAttributes]()
            
            // Duyệt các đối tượng trong attributeArray để tìm ra các cell nằm trong khung nhìn rect
            for attributes  in self.attributeArray {
                if (attributes.value as! UICollectionViewLayoutAttributes).frame.intersects(rect ) {
                    layoutAttributes.append((attributes.value as! UICollectionViewLayoutAttributes))
                }
            }
            return layoutAttributes
    }
    
    
    
//    override func layoutAttributesForItem(at indexPath: IndexPath)
//        -> UICollectionViewLayoutAttributes? {
//            print("layoutAttributesForItem")
//      return cache[indexPath.item]
//    }
}
