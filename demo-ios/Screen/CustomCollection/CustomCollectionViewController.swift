//
//  CustomCollectionViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 01/12/2023.
//

import UIKit
import AVFoundation


class CustomCollectionViewController: UIViewController, UICollectionViewDelegate, PinterestLayoutDelegate {
    let itemsPerRow = 2
    let screenSize = UIScreen.main.bounds
    let listImage = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"]
    @IBOutlet weak var collectionViewCustom: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if let layout = self.collectionViewCustom?.collectionViewLayout as? CustomLayout {
//            layout.delegateCustom = self
//        }
        let layout = CustomLayout()
        collectionViewCustom.setCollectionViewLayout(layout, animated: false)
        layout.delegateCustom = self
        let nib = UINib(nibName: "CustomCollectionViewCell", bundle: nil)
        collectionViewCustom.register(nib, forCellWithReuseIdentifier: "CustomCollectionViewCell")
        collectionViewCustom.dataSource = self
        collectionViewCustom.delegate = self
        // Do any additional setup after loading the view.
       
        
        
        
//                if let layout = collectionViewCustom?.collectionViewLayout as? UICollectionViewFlowLayout {
//                            layout.scrollDirection = .horizontal
//                        }
//        collectionViewCustom.transform = CGAffineTransform.init(rotationAngle: (-(CGFloat)(Double.pi)))
        
        //move scroll indicator from right to left
//        collectionViewCustom.scrollIndicatorInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: collectionViewCustom.bounds.size.width - 7)
        //        let layout = collectionViewCustom.collectionViewLayout as? CustomLayout
        //        layout?.delegateCustom  = self
        
    }
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        let paddingSpace = CGFloat(self.itemsPerRow + 1)
        let availableWidth = self.collectionViewCustom.frame.width - paddingSpace
        let widthPerItem = availableWidth / CGFloat(itemsPerRow)
        
        let boundingRect =  CGRect(x: 0, y: 0, width: widthPerItem, height: CGFloat.greatestFiniteMagnitude);
        let rect = AVMakeRect(aspectRatio: (UIImage(named: listImage[indexPath.item])?.size)!, insideRect: boundingRect);
        return rect.height
    }
    
}
extension CustomCollectionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewCustom.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath as IndexPath) as! CustomCollectionViewCell
        
        
        cell.viewCustomCell.backgroundColor = UIColor.random
        cell.image.image = UIImage(named: listImage[indexPath.row])
//        cell.label.text = "\(indexPath)"
        return cell
    }

}


extension CustomCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let colum = 2
        let width = screenSize.width / CGFloat(colum) - 10
        let heigh = [50, 70, 100, 200, 50, 150, 50, 70, 100, 200, 50, 150, 50, 70, 100, 200, 50, 150]
        let he = heigh[indexPath.row]
        return CGSize(width: CGFloat(width), height: CGFloat(he))
    }
}



extension UIColor {
    static var random: UIColor {
        return UIColor(
            red: .random(in: 0...1),
            green: .random(in: 0...1),
            blue: .random(in: 0...1),
            alpha: 1.0
        )
    }
}
