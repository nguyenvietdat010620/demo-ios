//
//  AppDelegate.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 30/10/2023.
//

import UIKit
import CoreData
import SQLite
import SQLite3
import GoogleMaps
import Alamofire
import IQKeyboardManagerSwift
import RealmSwift
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var db: OpaquePointer?
    var dataDetail: ResponseDetail?
    func openDatabase() {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("sqlitedemo.sqlite")
        
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("Error opening database")
            return
        }
        else{
            print(fileURL)
        }
    }
    
    func getData() {
        DetailService.shared.getDetailService { [weak self] result in
            switch result {
            case .success(let posts):
                self?.dataDetail = posts
                let realm = try! Realm()
                let data = DetailModalRealm()
                data.id = posts.data.id
                data.category_id = posts.data.category_id
                data.code = posts.data.code
                data.name = posts.data.name
                data.slug = posts.data.slug
                data.content = posts.data.content
                data.picture = "https://images.vietnamtourism.gov.vn/vn//images/2021/ho_guom.jpg"
                data.from_date = posts.data.from_date
                data.amount = posts.data.amount
                data.to_date = posts.data.to_date
                data.type = posts.data.type
                data.kind = posts.data.kind
                data.created_at = posts.data.created_at
                data.link = posts.data.link
                data.type_name = posts.data.type_name
                data.amount_text = posts.data.amount_text
                data.is_bookmark = posts.data.is_bookmark
                try! realm.write {
                    realm.add(data)
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey("AIzaSyCeGwOVt5mp3G7FYT0NOr5-odzmvD3vkXQ")
        databaseSQLite.shared.createTableArtical()
        databaseSQLite.shared.createTableDoctor()
        databaseSQLite.shared.createTablePromotion()
//        UILabel.appearance().font = UIFont(name: "NunitoSans", size: 15)
//        checkConnectingNetwork()
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 40
        IQKeyboardManager.shared.previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysHide
        getData()
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "demo_ios")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

