//
//  UserService.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 21/11/2023.
//

import Foundation
import Alamofire
class UserService:NSObject {
    static let shared: UserService = UserService()
    
    func getUserInfor(completion: @escaping (Result<UserResponse, Error>) -> Void) {
        AF.request("https://gist.githubusercontent.com/hdhuy179/7883b8f11ea4b25cf6d3822c67049606/raw/Training_Intern_BasicApp_UserInfo", method: .get)
            .responseDecodable(of: UserResponse.self) { response in
                switch response.result {
                case .success(let posts):
                    completion(.success(posts))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
    }
    
}
