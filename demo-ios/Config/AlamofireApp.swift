//
//  AlamofireApp.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 24/11/2023.
//

import Foundation
import Alamofire

class NetworkService {
    static let shared: NetworkService = NetworkService()
    
    lazy var timeout: TimeInterval = 45 // seconds
    
    // Temporarily avoiding refresh token - 401 infinite loop by tracking if logic is looping
    var hasBeenError: Bool = false
    
    func getUserInfor(completion: @escaping (Result<UserResponse, Error>) -> Void) {
        AF.request("https://gist.githubusercontent.com/hdhuy179/7883b8f11ea4b25cf6d3822c67049606/raw/Training_Intern_BasicApp_UserInfo", method: .get,
            interceptor: self
        )
            .responseDecodable(of: UserResponse.self) { response in
                switch response.result {
                case .success(let posts):
                    completion(.success(posts))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
    }
}

enum NetworkErrorInter: Error {
    case notConnectedToInternet
    
}

extension NetworkService: RequestInterceptor {
    func adapt(
      _ urlRequest: URLRequest,
      for session: Session,
     completion: @escaping (Result<URLRequest, Error>) -> Void
    ) {
        
     // Called before request to set request headers
    }
    
    
    func retry(
      _ request: Request,
      for session: Session,
      dueTo error: Error,
      completion: @escaping (RetryResult) -> Void) {

         let reachabilityManager = Alamofire.NetworkReachabilityManager()?.startListening(onUpdatePerforming: { status in
                    print("reachabilityManager", status)
                    switch status {
                    case .reachable(.ethernetOrWiFi):
//                        NetworkError.shared.showErrorNetwork()
                        completion(.doNotRetryWithError(NetworkErrorInter.notConnectedToInternet))
                        print("ethernetOrWiFi")
                    case .unknown:
                        print("ethernetOrWiFi")
//                        NetworkError.shared.showErrorNetwork()
                        print("ethernetOrWiFi")
                    case .notReachable:
//                        print("day ne")
 //                       NetworkError.shared.showErrorNetwork()
 //                       Loading.shared.showLoading(view: self.view)
                        print("ethernetOrWiFi")
                        
                        completion(.doNotRetryWithError(NetworkErrorInter.notConnectedToInternet))
                    case .reachable(_):
                        print("ethernetOrWiFi")
//                        NetworkError.shared.showErrorNetwork()
                    }
                })
         
      // Called when status code is not 200...299 or in failure
    }

}
