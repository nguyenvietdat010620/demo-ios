//
//  DoctorModel.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 20/11/2023.
//

import Foundation

class DoctorModel: Codable {
    var id: Int
    var full_name: String?
    var name: String
    var last_name: String
    var birth_date: String
    var contact_email: String
    var phone: String
    var avatar: String
    var blood_name: String
    var sex_name: String
    var majors_name: String
    var user_type_name: String
    var ratio_star: Float
    var number_of_reviews: Int
    var number_of_stars: Int
    var working_hour: String
    var training_place: String
    var degree: String
    var created_at: String
}

class DoctorData: Codable {
    var items: [DoctorModel]
    var total_record: Int
    var total_page: Int
    var page: Int
}

class DoctorResponse: Codable {
    var status: Int
    var message: String
    var code: Int
    var data: DoctorData?
}
