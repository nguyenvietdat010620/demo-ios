//
//  DoctorViewModel.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 20/11/2023.
//

import Foundation

protocol DoctorViewModelProtocol {
    func getListDoctor(data: DoctorResponse)
}

class DoctorViewModel {
    
    var delegate: DoctorViewModelProtocol?
    
    init(view: DoctorViewModelProtocol) {
        delegate = view
        
    }
    
    func getListDoctor() {
        DoctorService.shared.getDoctorService { [weak self] response in
            switch response {
            case .success(let data):
                self?.delegate?.getListDoctor(data: data)
            case .failure(let err):
                print("NewsService err", err)
            default:
                break
            }
        }
    }
}
