//
//  DoctorService.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 20/11/2023.
//

import Foundation
import Alamofire

class DoctorService: NSObject {
    static let shared: DoctorService = DoctorService()
    
    func getDoctorService(completion: @escaping (Result<DoctorResponse, Error>) -> Void) {
        AF.request("https://gist.githubusercontent.com/hdhuy179/9ac0a89969b46fb67bc7d1a8b94d180e/raw", method: .get)
            .responseDecodable(of: DoctorResponse.self) { response in
                print("response.result", response.result)
                switch response.result {
                case .success(let doctors):
                    completion(.success(doctors))
                case .failure(let error):
                    completion(.failure(error))
                    print("false post", error)
                }
            }
    }
    
}
