//
//  DoctorTableViewCell.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 23/11/2023.
//

import UIKit

class DoctorTableViewCell: UITableViewCell {

    @IBOutlet weak var numberOfReview: UILabel!
    @IBOutlet weak var numberOfStar: UILabel!
    @IBOutlet weak var majorDoctor: UILabel!
    @IBOutlet weak var nameDoctor: UILabel!
    @IBOutlet weak var imageAvataDoctor: UIImageView!
    @IBOutlet weak var viewBorder: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBorder.layer.cornerRadius = 12
        viewBorder.layer.borderWidth = 1
        viewBorder.layer.borderColor = UIColor(red: 150/256, green: 155/256, blue: 171/256, alpha: 1).cgColor
        imageAvataDoctor.layer.cornerRadius = 5
        imageAvataDoctor.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
