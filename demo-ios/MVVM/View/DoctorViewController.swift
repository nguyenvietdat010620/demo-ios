//
//  DoctorViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 20/11/2023.
//

import UIKit

extension DoctorViewController: DoctorViewModelProtocol {
    func getListDoctor(data: DoctorResponse) {
        self.dataResDoctor = data
        tableDoctor.reloadData()
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { time in
            Loading.shared.hideOverlayView()
        }
    }
    
}
class DoctorViewController: UIViewController {
    
    @IBOutlet weak var tableDoctor: UITableView!
    var dataResDoctor: DoctorResponse?
    var viewModel: DoctorViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = DoctorViewModel(view: self)
        
        let nib = UINib(nibName: "DoctorTableViewCell", bundle: nil)
        tableDoctor.register(nib, forCellReuseIdentifier: "DoctorTableViewCell")
        tableDoctor.dataSource = self
        tableDoctor.delegate = self
        tableDoctor.separatorStyle = .none
        viewModel?.getListDoctor()
        Loading.shared.showLoading(view: self.view)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shareButton(_ sender: Any) {
        let text = "This is some text that I want to share."
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension DoctorViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataResDoctor?.data?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorTableViewCell", for: indexPath as IndexPath) as! DoctorTableViewCell
        if(dataResDoctor?.data?.items[indexPath.row].avatar != "") {
            let imageURL = dataResDoctor?.data?.items[indexPath.row].avatar ?? ""
            ImageLoader.loadImage(from: imageURL) { image in
                DispatchQueue.main.async {
                    cell.imageAvataDoctor.image = image
                }
            }
        }
        else {
            cell.imageAvataDoctor.image = UIImage(named: "doctor")
        }
        cell.majorDoctor.text = dataResDoctor?.data?.items[indexPath.row].majors_name
        cell.nameDoctor.text = dataResDoctor?.data?.items[indexPath.row].full_name
        cell.numberOfReview.text = "\(dataResDoctor?.data?.items[indexPath.row].number_of_reviews ?? 0)"
        cell.numberOfStar.text = "\(dataResDoctor?.data?.items[indexPath.row].ratio_star ?? 0)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
}
