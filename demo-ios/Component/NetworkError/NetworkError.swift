//
//  NetworkError.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 24/11/2023.
//

import Foundation
import UIKit

public class NetworkError: UIViewController {
    var viewMain = UIView()
    var imageErr = UIImageView()
    let screenSize = UIScreen.main.bounds
    
    class var shared: NetworkError {
        struct Static {
            static let instance: NetworkError = NetworkError()
        }
        return Static.instance
    }
    
    func showErrorNetwork(view: UIView) {
        let colorTop = UIColor.white.cgColor
        let colorBottom = UIColor(red: 0.941 , green: 0.818, blue: .zero, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        
        viewMain.frame = CGRect(x:0, y: 0, width:screenSize.width, height:screenSize.height)
        viewMain.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        viewMain.clipsToBounds = true
        viewMain.layer.cornerRadius = 10
        
        gradientLayer.frame = viewMain.bounds
        gradientLayer.colors = [colorTop, colorBottom]
        viewMain.layer.insertSublayer(gradientLayer, at: 0)
        
        imageErr.image = UIImage(named: "doctor")
        imageErr.frame = CGRect(x:0, y: 0, width:60, height:60)
        imageErr.center = CGPoint(x: viewMain.bounds.width / 2, y: viewMain.bounds.height / 2)
        
        viewMain.addSubview(imageErr)
        view.addSubview(viewMain)
    }
    
    public func hideNetworkError() {
        viewMain.removeFromSuperview()
    }
}
