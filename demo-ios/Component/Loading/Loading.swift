//
//  Loading.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 23/11/2023.
//

import Foundation
import UIKit

public class Loading: UIViewController {
    var overlayView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    let screenSize = UIScreen.main.bounds
    
    class var shared: Loading {
        struct Static {
            static let instance: Loading = Loading()
        }
        return Static.instance
    }
    
    public func showLoading(view: UIView) {
        let colorTop = UIColor.white.cgColor
        let colorBottom = UIColor(red: 0.941 , green: 0.818, blue: .zero, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        
        overlayView.frame = CGRect(x:0, y: 0, width:screenSize.width, height:screenSize.height)
        overlayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        overlayView.clipsToBounds = true
        overlayView.layer.cornerRadius = 10
        
        gradientLayer.frame = overlayView.bounds
        gradientLayer.colors = [colorTop, colorBottom]
        overlayView.layer.insertSublayer(gradientLayer, at: 0)
        
        activityIndicator.frame = CGRect(x:0, y: 0, width:40, height:40)
        activityIndicator.style = .whiteLarge
        activityIndicator.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
        
        overlayView.addSubview(activityIndicator)
        view.addSubview(overlayView)
        activityIndicator.startAnimating()
    }
    
    public func hideOverlayView() {
        activityIndicator.stopAnimating()
        overlayView.removeFromSuperview()
    }
}
