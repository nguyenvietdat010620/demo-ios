//
//  SQLData.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 13/11/2023.
//

import Foundation
import SQLite
import SQLite3
class databaseSQLite {
    static let shared = databaseSQLite()
    var db: OpaquePointer?
    
        func openDatabase() {
            let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                .appendingPathComponent("sqlitedemo.sqlite")
    
            if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
                print("Error opening database")
                return
            }
            else{
                print(fileURL)
            }
        }
   
    func createTableArtical() {
        let createTableString = """
        CREATE TABLE Artical(
        id INT PRIMARY KEY NOT NULL,
        category_id INT,
        title TEXT,
        slug TEXT,
        picture TEXT,
        picture_caption TEXT,
        created_at TEXT,
        category_name TEXT,
        link TEXT);
        """
      // 1
        openDatabase()
      var createTableStatement: OpaquePointer?
      // 2
      if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) ==
          SQLITE_OK {
        // 3
        if sqlite3_step(createTableStatement) == SQLITE_DONE {
          print("\nContact table created.")
        } else {
          print("\nContact table is not created.")
        }
      } else {
        print("\nCREATE TABLE statement is not prepared.")
      }
      // 4
      sqlite3_finalize(createTableStatement)
    }
    
    func createTableDoctor() {
        let createTableString = """
        CREATE TABLE Doctor(
        id INT PRIMARY KEY NOT NULL,
        full_name TEXT,
        name TEXT,
            last_name TEXT,
            contact_email TEXT,
            phone TEXT,
            avatar TEXT,
            majors_name TEXT,
            ratio_star FLOAT,
            number_of_reviews INT,
            number_of_stars INT);
        """
      // 1
        openDatabase()
      var createTableStatement: OpaquePointer?
      // 2
      if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) ==
          SQLITE_OK {
        // 3
        if sqlite3_step(createTableStatement) == SQLITE_DONE {
          print("\nDoctor table created.")
        } else {
          print("\nDoctor table is not created.")
        }
      } else {
        print("\nDoctor TABLE statement is not prepared.")
      }
      // 4
      sqlite3_finalize(createTableStatement)
    }
    
    func createTablePromotion() {
        let createTableString = """
        CREATE TABLE Promotion(
        id INT PRIMARY KEY NOT NULL,
            category_id INT,
            code TEXT,
                name TEXT,
                slug TEXT,
                content TEXT,
                picture TEXT,
                from_date TEXT,
                            to_date TEXT,
                            amount INT,
                            type INT,
                kind INT,
                            created_at TEXT,
                            category_name TEXT,
                            amount_text TEXT,
                            link TEXT,
                is_bookmark Boolean);
        """
      // 1
        openDatabase()
      var createTableStatement: OpaquePointer?
      // 2
      if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) ==
          SQLITE_OK {
        // 3
        if sqlite3_step(createTableStatement) == SQLITE_DONE {
          print("\nPromotion table created.")
        } else {
          print("\nPromotion table is not created.")
        }
      } else {
        print("\nPromotion TABLE statement is not prepared.")
      }
      // 4
      sqlite3_finalize(createTableStatement)
    }
    
    func insertIntoTableDoctor(id: Int, full_name: String, name: String, last_name: String, contact_email: String, phone: String, avatar: String, majors_name: String, ratio_star: Float, number_of_reviews: Int,number_of_stars: Int ) {
        let insertStatementString = "INSERT INTO Doctor (id, full_name, name, last_name, contact_email, phone, avatar, majors_name, ratio_star, number_of_reviews, number_of_stars) VALUES ('\(id)', '\(full_name)', '\(name)', '\(last_name)', '\(contact_email)', '\(phone)', '\(avatar)', '\(majors_name)', '\(ratio_star)', '\(number_of_reviews)', '\(number_of_stars)');"
        var insertStatement: OpaquePointer?
          // 1
          if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) ==
              SQLITE_OK {
            let id: Int32 = 1
            let name: NSString = "Ray"
            // 2
            sqlite3_bind_int(insertStatement, 1, id)
            // 3
            sqlite3_bind_text(insertStatement, 2, name.utf8String, -1, nil)
            // 4
            if sqlite3_step(insertStatement) == SQLITE_DONE {
              print("\nSuccessfully inserted row.")
            } else {
              print("\nCould not insert row.")
            }
          } else {
            print("\nINSERT statement is not prepared.")
          }
          // 5
          sqlite3_finalize(insertStatement)
    }
    
    func insertIntoTableArtical(id: Int, category_id: Int, title: String, slug: String, picture: String, picture_caption: String, created_at:String, category_name: String, link:String ) {
        let insertStatementString = "INSERT INTO Artical (id, category_id, title, slug, picture, picture_caption, created_at, category_name, link) VALUES ('\(id)', '\(category_id)', '\(title)', '\(slug)', '\(picture)', '\(picture_caption)', '\(created_at)', '\(category_name)', '\(link)')"
        var insertStatement: OpaquePointer?
          // 1
          if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) ==
              SQLITE_OK {
            let id: Int32 = 1
            let name: NSString = "Ray"
            // 2
            sqlite3_bind_int(insertStatement, 1, id)
            // 3
            sqlite3_bind_text(insertStatement, 2, name.utf8String, -1, nil)
            // 4
            if sqlite3_step(insertStatement) == SQLITE_DONE {
              print("\nSuccessfully inserted row.")
            } else {
              print("\nCould not insert row.")
            }
          } else {
            print("\nINSERT statement is not prepared.")
          }
          // 5
          sqlite3_finalize(insertStatement)
    }
    
    func insertIntoTablePromotion(id:Int, category_id:Int, code: String, name : String, slug: String, content: String, picture: String, from_date: String, to_date: String, amount :Int, type :Int, kind:Int, created_at: String, category_name: String, amount_text: String, link: String, is_bookmark: Bool) {
        let insertStatementString = "INSERT INTO Promotion (id, category_id, code, name, slug, content, picture, from_date, to_date,amount, type, kind, created_at, category_name, amount_text, link, is_bookmark) VALUES ('\(id)', '\(category_id)', '\(code)', '\(name)', '\(slug)', '\(content)', '\(picture)', '\(from_date)', '\(to_date)', '\(amount)', '\(type)', '\(kind)', '\(created_at)', '\(category_name)', '\(amount_text)', '\(link)', '\(is_bookmark)') ;"
        var insertStatement: OpaquePointer?
          // 1
          if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) ==
              SQLITE_OK {
            let id: Int32 = 1
            let name: NSString = "Ray"
            // 2
            sqlite3_bind_int(insertStatement, 1, id)
            // 3
            sqlite3_bind_text(insertStatement, 2, name.utf8String, -1, nil)
            // 4
            if sqlite3_step(insertStatement) == SQLITE_DONE {
              print("\nSuccessfully inserted row.")
            } else {
              print("\nCould not insert row.")
            }
          } else {
            print("\nINSERT statement is not prepared.")
          }
          // 5
          sqlite3_finalize(insertStatement)
    }
    
    func selectDoctor() {
        let queryStatementString = "SELECT * FROM Doctor;"
          var queryStatement: OpaquePointer?
          // 1
          if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) ==
              SQLITE_OK {
            // 2
            while sqlite3_step(queryStatement) == SQLITE_ROW {
              // 3
              let id = sqlite3_column_int(queryStatement, 0)
                guard let queryResultCol1 = sqlite3_column_text(queryStatement, 1) else {
                //                print("Query result is nil")
                                return
                              }
              let full_name = String(cString: queryResultCol1)
              // 5
              print("\nQuery Result:")
              print("\(id) | \(full_name) | \(queryResultCol1)")
          }
          }
//                else {
//              print("\nQuery returned no results.")
//          }
//          } else {
//              // 6
//            let errorMessage = String(cString: sqlite3_errmsg(db))
//            print("\nQuery is not prepared \(errorMessage)")
//          }
          // 7
          sqlite3_finalize(queryStatement)
        

    }
}
