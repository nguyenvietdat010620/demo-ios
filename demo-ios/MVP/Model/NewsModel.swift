//
//  NewsModel.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 17/11/2023.
//

import Foundation

class NewsModel: Codable {
    var id: Int
    var category_id: Int
    var title: String
    var slug: String
    var summary: String
    var picture: String
    var picture_caption: String
    var created_at: String
    var category_name: String
    var link: String
    var isSave: Bool? = false
}

class ItemData: Codable {
    var items: [NewsModel]
}

class NewResponse: Codable {
    var status: Int
    var message: String
    var code: Int
    var data: ItemData
}
