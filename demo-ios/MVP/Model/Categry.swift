////
////  NewsModel.swift
////  demo-ios
////
////  Created by Nguyen Viet Dat on 17/11/2023.
////
//
//import Foundation
//struct Category {
//    let id: Int
//    let name: String
//}
//
//protocol ICategoryService {
//    func getCategoryById(id: Int) -> Category?
//
//}
//
//class CategoryService: ICategoryService {
//    func getCategoryById(id: Int) -> Category? {
//        let categories = [Category(id: 1, name: "Movies"),
//                          Category(id: 2, name: "Books"),
//                          Category(id: 3, name: "Computer")]
//
//        let category = categories.filter({
//            $0.id == id
//        }).first
//
//        return category
//    }
//}
