//
//  ViewPresenter.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 15/11/2023.
//

import Foundation
import Alamofire

protocol PresenterProtocol {
    func actionSave(item: NewResponse, index: Int)
    func getData(data: NewResponse)
}

class HomeViewPresenter {
    // 1
    var delegate: PresenterProtocol?
    var dataNew: NewResponse?
    
    init(view: PresenterProtocol) {
        delegate = view
    }
    
    func saveActionItem(index: Int, data: NewResponse) {
        let copydataNew = data
        let itemCopy = copydataNew.data.items[index]
        let itemNewsCopy: NewsModel = itemCopy
        itemNewsCopy.isSave = itemNewsCopy.isSave == false ?  true : false
        copydataNew.data.items[index] = itemNewsCopy
        delegate?.actionSave(item: copydataNew, index: index)
    }
    
    func getData() {
        NewsService.shared.getNewsService {  [weak self] response in
            switch response {
            case .success(let data):
                self?.dataNew = data
                self?.delegate?.getData(data: data)
            case .failure(let err):
                print("NewsService err", err)
            default:
                break
            }
        }
    }
}
