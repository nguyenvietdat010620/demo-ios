//
//  NewsTableViewCell.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 17/11/2023.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageSave: UIButton!
    @IBOutlet weak var titleDateItemNew: UILabel!
    @IBOutlet weak var labelTitleItemNew: UILabel!
    @IBOutlet weak var imageItemNews: UIImageView!
    @IBOutlet weak var buttonSave: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
