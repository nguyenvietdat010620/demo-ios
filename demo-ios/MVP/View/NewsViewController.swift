//
//  NewsViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 15/11/2023.
//

import UIKit


extension NewsViewController: PresenterProtocol {
    func actionSave(item: NewResponse, index: Int) {
        dataNew = item
        let indexPath = IndexPath(item: index, section: 0)
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    func getData(data: NewResponse) {
        dataNew = data
        tableView.reloadData()
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { time in
            Loading.shared.hideOverlayView()
        }
    }
    
}

class NewsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageMain: UIImageView!
    @IBOutlet weak var labelTest: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var objNew: ModalArtical?
    var presenter: HomeViewPresenter!
    var dataNew: NewResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = HomeViewPresenter(view: self)
//        getData()
        let nib = UINib(nibName: "NewsTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "NewsTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        
        presenter?.getData()
        Loading.shared.showLoading(view: self.view)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataNew?.data.items.count ?? 1
    }
    
    @IBAction func buttonBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell", for: indexPath as IndexPath) as! NewsTableViewCell
        let imageURL = "https://images.vietnamtourism.gov.vn/vn//images/2021/ho_guom.jpg"
        ImageLoader.loadImage(from: imageURL) { image in
            DispatchQueue.main.async {
                cell.imageItemNews.image = image
            }
        }
        cell.labelTitleItemNew.text = dataNew?.data.items[indexPath.row].title
        cell.titleDateItemNew.text = dataNew?.data.items[indexPath.row].created_at
        cell.buttonSave.setImage(dataNew?.data.items[indexPath.row].isSave == false ? UIImage(named: "bookmarkcolor") : UIImage(named: "bookmark"),  for: .normal)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case indexPath.row:
            presenter.saveActionItem(index: indexPath.row, data: dataNew!)
        default:
            print()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 280))
        let imageMain = UIImageView()
        let imageURL =  "https://images.vietnamtourism.gov.vn/vn//images/2021/ho_guom.jpg"
        ImageLoader.loadImage(from: imageURL) { image in
            DispatchQueue.main.async {
                imageMain.image = image
            }
        }
        imageMain.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 200)
        let viewText = UIView()
        viewText.frame = CGRect(x: 0, y: 200, width: tableView.frame.width, height: 100)
        
        let labelTitle = UILabel()
        labelTitle.frame = CGRect.init(x: 16, y: 10, width: viewText.frame.width-16, height: 40)
        //        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        labelTitle.text = objNew?.title ?? ""
        labelTitle.numberOfLines = 0
        labelTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 15)
        
        let labelDate = UILabel()
        labelDate.text = objNew?.created_at ?? ""
        labelDate.frame = CGRect.init(x: 16, y: 50, width: headerView.frame.width-16, height: 20)
        
        viewText.addSubview(labelTitle)
        viewText.addSubview(labelDate)
        headerView.addSubview(viewText)
        headerView.addSubview(imageMain)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
}
