//
//  NewService.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 17/11/2023.
//

import Foundation
import Alamofire

class NewsService: NSObject {
    static let shared: NewsService = NewsService()
    
    func getNewsService(completion: @escaping (Result<NewResponse, Error>) -> Void) {
        AF.request("https://gist.github.com/hdhuy179/84d1dfe96f2c0ab1ddea701df352a7a6/raw", method: .get)
            .responseDecodable(of: NewResponse.self) { response in
                switch response.result {
                case .success(let posts):
                    completion(.success(posts))
                case .failure(let error):
                    completion(.failure(error))
                    print("false post", error)
                }
            }
    }
    
}
