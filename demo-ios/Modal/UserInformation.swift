//
//  UserInformation.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 21/11/2023.
//

import Foundation
class UserInformation: Codable {
    var id: Int
    var name: String?
    var last_name: String?
    var username: String?
    var contact_email: String?
    var phone: String?
    var card_id: String?
    var address: String?
    var province_code: String?
    var district_code: String?
    var ward_code: String?
    var latitude: Int?
    var longitude: Int?
    var birth_date: String?
    var avatar: String?
    var degree: String?
    var training_place: String?
    var academic_rank: String?
    var majors_id: Int?
    var hospital_name: String?
    var sex: Int?
    var blood: Int?
    var description_self: String?
    var verified_at: String?
    var current_step: Int?
    var user_type: Int?
    var refer_code: String?
    var working_hour_type: Int?
    var balance: Int?
    var ratio_star: Float?
    var number_of_reviews: Int?
    var number_of_stars: Int?
    var status: Int?
    var is_first_login: Int?
    var created_at: String?
    var updated_at: String?
    var full_address: String?
    var full_name: String?
    var order_cancel_total: Int?
    var referral_total: Int?
    var blood_name: String?
    var total_appointment: Int?
}

class UserResponse: Codable {
    var status: Int
    var message: String
    var code: Int
    var data: UserInformation
}
