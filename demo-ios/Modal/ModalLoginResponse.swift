//
//  ModalLoginResponse.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 01/11/2023.
//

import Foundation

// MARK: - Welcome
struct Welcome: Codable {
    let errorCode: Int
    let message: String
    let data: DataClass?
}

// MARK: - DataClass
struct DataClass: Codable {
    let accessToken, refreshToken, msisdn: String
    let userID, profileID, expiryDuration: Int
    let contentFilter, contentFilterValue: String
    let needChangePassword: Bool
    let name, urlAvatar: String
    let otpToken: String?
    
    enum CodingKeys: String, CodingKey {
        case accessToken, refreshToken, msisdn
        case userID = "userId"
        case profileID = "profileId"
        case expiryDuration, contentFilter, contentFilterValue, needChangePassword, name, urlAvatar, otpToken
    }
    
    init(
        accessToken: String,
        refreshToken: String,
        msisdn: String,
        userID: Int,
        profileID: Int,
        expiryDuration: Int,
        contentFilter: String,
        contentFilterValue: String,
        needChangePassword: Bool,
        name: String,
        urlAvatar: String,
        otpToken: String?
    ) {
        self.accessToken = accessToken
        self.refreshToken = refreshToken
        self.msisdn = msisdn
        self.userID = userID
        self.profileID = profileID
        self.expiryDuration = expiryDuration
        self.contentFilter = contentFilter
        self.contentFilterValue = contentFilterValue
        self.needChangePassword = needChangePassword
        self.name = name
        self.urlAvatar = urlAvatar
        self.otpToken = otpToken
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(accessToken, forKey: .accessToken)
        try container.encode(refreshToken, forKey: .refreshToken)
        try container.encode(msisdn, forKey: .msisdn)
        try container.encode(userID, forKey: .userID)
        try container.encode(profileID, forKey: .profileID)
        try container.encode(expiryDuration, forKey: .expiryDuration)
        try container.encode(contentFilter, forKey: .contentFilter)
        try container.encode(contentFilterValue, forKey: .contentFilterValue)
        try container.encode(needChangePassword, forKey: .needChangePassword)
        try container.encode(name, forKey: .name)
        try container.encode(urlAvatar, forKey: .urlAvatar)
        try container.encode(otpToken, forKey: .otpToken)
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.accessToken = try container.decode(String.self, forKey: .accessToken)
        self.refreshToken = try container.decode(String.self, forKey: .refreshToken)
        self.msisdn = try container.decode(String.self, forKey: .msisdn)
        self.userID = try container.decode(Int.self, forKey: .userID)
        self.profileID = try container.decode(Int.self, forKey: .profileID)
        self.expiryDuration = try container.decode(Int.self, forKey: .expiryDuration)
        self.contentFilter = try container.decode(String.self, forKey: .contentFilter)
        self.contentFilterValue = try container.decode(String.self, forKey: .contentFilterValue)
        self.needChangePassword = try container.decode(Bool.self, forKey: .needChangePassword)
        self.name = try container.decode(String.self, forKey: .name)
        self.urlAvatar = try container.decode(String.self, forKey: .urlAvatar)
        self.otpToken = try? container.decodeIfPresent(String.self, forKey: .otpToken)
    }
}
