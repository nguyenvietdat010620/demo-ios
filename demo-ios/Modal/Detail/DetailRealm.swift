//
//  DetailRealm.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 15/11/2023.
//

import Foundation
import RealmSwift

class DetailModalRealm: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var category_id: Int = 0
    @objc dynamic var code: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var slug: String = ""
    @objc dynamic var content: String = ""
    @objc dynamic var picture: String = ""
    @objc dynamic var from_date: String = ""
    @objc dynamic var amount: Int = 0
    @objc dynamic var to_date: String = ""
    @objc dynamic var type: Int = 0
    @objc dynamic var kind: Int = 0
    @objc dynamic var created_at: String = ""
    @objc dynamic var link: String = ""
    @objc dynamic var type_name: String = ""
    @objc dynamic var amount_text: String = ""
    @objc dynamic var is_bookmark: Bool = false
    
    convenience init(id: Int, category_id: Int, code:String, name:String,slug:String,content:String,picture:String,from_date:String,amount:Int,to_date:String,type:Int, kind:Int, created_at:String,link:String, type_name:String, amount_text:String,is_bookmark:Bool) {
        self.init()
        self.id = id
        self.category_id = category_id
        self.code = code
        self.name = name
        self.slug = slug
        self.content = content
        self.picture = picture
        self.from_date = from_date
        self.amount = amount
        self.to_date = to_date
        self.type = type
        self.kind = kind
        self.created_at = created_at
        self.link = link
        self.type_name = type_name
        self.amount_text = amount_text
        self.is_bookmark = is_bookmark
    }
}

