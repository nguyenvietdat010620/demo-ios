//
//  DetailModel.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 14/11/2023.
//

import Foundation

class DetailModal: Codable {
    var id: Int
    var category_id: Int
    var code: String
    var name: String
    var slug: String
    var content: String
    var picture: String
    var from_date: String
    var amount: Int
    var to_date: String
    var type: Int
    var kind: Int
    var created_at: String
    var link: String
    var type_name: String
    var amount_text: String
    var is_bookmark: Bool
}

class ResponseDetail: Codable {
    var status: Int
    var message: String
    var code: Int
    var data: DetailModal
}
